extern crate byteorder;
extern crate clap;
extern crate crypto;

use byteorder::{BigEndian, ReadBytesExt};
use clap::{App, Arg, SubCommand};
use crypto::{digest::Digest, sha1::Sha1};
use std::{fs, io, io::Cursor, path::Path};

#[derive(Debug)]
struct IndexEntry {
    ctime_sec: u32,
    ctime_nano_sec: u32,
    mtime_sec: u32,
    mtime_nano_sec: u32,
    dev: u32,
    ino: u32,
    mode: u32,
    uid: u32,
    gid: u32,
    file_size: u32,
    checksum: Vec<u8>,
    flags: u16,
    path_name: String,
}

fn init_repo(repo_name: &str) -> io::Result<String> {
    fs::create_dir(repo_name)?;

    fs::create_dir(Path::new(repo_name).join(".git"))?;

    for init_object in ["objects", "refs", "refs/heads"].iter() {
        fs::create_dir(Path::new(repo_name).join(".git").join(init_object))?;
    }

    fs::write(
        Path::new(repo_name).join(".git/HEAD"),
        b"ref: refs/heads/master",
    )?;

    println!("Initialized empty repsitory: {}", repo_name);

    Ok(Path::new("repo_name").to_str().unwrap().to_string())
}

fn hash_object(object_name: &str, object_type: &str, write: bool) -> String {
    println!("{} {}", object_name, object_type);

    String::new()
}

fn read_index() -> io::Result<Vec<IndexEntry>> {
    let index_path = Path::new(".git").join("index");

    if !index_path.exists() {
        return Ok(vec![]);
    }

    let index_data = fs::read(index_path)?;

    let mut sha1_hasher = Sha1::new();

    sha1_hasher.input(&index_data[..index_data.len() - 20]);

    let mut digest = vec![0u8; 20];

    sha1_hasher.result(&mut digest);

    let index_digest = &index_data[index_data.len() - 20..];

    assert_eq!(
        digest, index_digest,
        "Index checksum verification failed. Possible index corruption!"
    );

    let header = &index_data[..12];

    let signature = String::from_utf8_lossy(&header[..4]);

    let mut rdr = Cursor::new(&header[4..]);

    let version = rdr.read_u32::<BigEndian>().unwrap();

    let entries_count = rdr.read_u32::<BigEndian>().unwrap();

    assert_eq!("DIRC", signature);

    assert_eq!(2, version);

    let entries_data = &index_data[12..index_data.len() - 20];

    let mut entry_start = 0;

    let mut index_entries = vec![];

    while entry_start + 62 < entries_data.len() {
        let fields_end = entry_start + 62;

        let fields_data = &entries_data[entry_start..fields_end];

        let mut rdr = Cursor::new(fields_data);

        let ctime_sec = rdr.read_u32::<BigEndian>().unwrap();

        let ctime_nano_sec = rdr.read_u32::<BigEndian>().unwrap();

        let mtime_sec = rdr.read_u32::<BigEndian>().unwrap();

        let mtime_nano_sec = rdr.read_u32::<BigEndian>().unwrap();

        let dev = rdr.read_u32::<BigEndian>().unwrap();

        let ino = rdr.read_u32::<BigEndian>().unwrap();

        let mode = rdr.read_u32::<BigEndian>().unwrap();

        let uid = rdr.read_u32::<BigEndian>().unwrap();

        let gid = rdr.read_u32::<BigEndian>().unwrap();

        let file_size = rdr.read_u32::<BigEndian>().unwrap();

        let mut rdr = Cursor::new(&fields_data[60..]);

        let flags = rdr.read_u16::<BigEndian>().unwrap();

        let checksum = fields_data[40..60].to_vec();

        let path_end = entries_data
            .iter()
            .enumerate()
            .position(|(i, x)| *x == 0 && i > fields_end)
            .unwrap();

        let path_name = String::from_utf8_lossy(&entries_data[fields_end..path_end]);

        let index_entry = IndexEntry {
            ctime_sec,
            ctime_nano_sec,
            mtime_sec,
            mtime_nano_sec,
            dev,
            ino,
            mode,
            uid,
            gid,
            file_size,
            checksum,
            flags,
            path_name: path_name.to_string(),
        };

        index_entries.push(index_entry);

        entry_start += ((62 + (path_end - fields_end) + 8) / 8) * 8;
    }

    assert_eq!(
        entries_count as usize,
        index_entries.len(),
        "Wrong number of entries read from the index!"
    );

    Ok(index_entries)
}

fn ls_files(stage: bool) {
    let index_entries = read_index().unwrap();

    index_entries.iter().for_each(|entry| {
        if stage {
            println!(
                "{:6o} {} {}\t{}",
                entry.mode,
                entry
                    .checksum
                    .iter()
                    .map(|x| format!("{:02x}", x))
                    .collect::<String>(),
                (entry.flags >> 12) & 3,
                entry.path_name
            );
        } else {
            println!("{}", entry.path_name);
        }
    });
}

fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .subcommand(
            SubCommand::with_name("init")
                .about("Initializes an empty Git repository")
                .arg(Arg::with_name("repo_name").help("Name of the initialized Git repository")),
        )
        .subcommand(
            SubCommand::with_name("ls-files")
                .about("Prints the list of files in the index")
                .arg(Arg::with_name("stage").short("s").long("stage").help(
                    "Show staged contents' mode bits, object name and stage number in the output.",
                )),
        )
        .subcommand(
            SubCommand::with_name("hash-object")
            .about("Computes the object ID value for an object with specified type with the contents of the named file.")
            .arg(Arg::with_name("object_type").takes_value(true).short("t").long("type"))
            .arg(Arg::with_name("object_name").required(true))
        )
        .get_matches();

    if let Some(init_match) = matches.subcommand_matches("init") {
        if let Some(repo_name) = init_match.value_of("repo_name") {
            init_repo(repo_name).unwrap();
        }
    } else if let Some(ls_files_match) = matches.subcommand_matches("ls-files") {
        let stage = ls_files_match.is_present("stage");

        ls_files(stage);
    } else if let Some(hash_object_match) = matches.subcommand_matches("hash-object") {
        let object_type = hash_object_match.value_of("type").unwrap_or("blob");

        let object_name = hash_object_match.value_of("object_name").unwrap();

        hash_object(object_name, object_type, false);
    }
}
